/*
* Uses jQuery!!!!111one
*/


/*====================================
=            ON DOM READY            =
====================================*/
$(function() {
  
    // Toggle Nav on Click
    $('.toggle-nav').click(function(event) {
        event.preventDefault();
        $(this).find('i').toggleClass('fa-close fa-bars');
        $(this).toggleClass('change');
        // Calling a function in case you want to expand upon this.
        //toggleNav();
        $('#site-menu').toggleClass('change');
    });

    $('.slider').owlCarousel({
        center: true,
        items:1,
        nav: true,
        loop:true,
        center: true,
        autoplay: 4500,
        slideSpeed: 1700,
        responsive: true,
        responsiveRefreshRate: 100,
        autoplayHoverPause: true,
        //stagePadding: 100,
        // animateOut: 'bounceOut',
        // animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            600:{items:1}
        }
    });

    $('.services-slider').owlCarousel({
        center: true,
        items:1,
        nav: true,
        loop:true,
        center: true,
        autoplay: false,
        slideSpeed: 1700,
        responsive: true,
        responsiveRefreshRate: 100,
        autoplayHoverPause: false,
        //stagePadding: 100,
        // animateOut: 'bounceOut',
        // animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            600:{items:1}
        }
    });

    //if heigher than 768 affix logo
    if($(window).width() > 768) {
        $('#site-wrapper').scroll(function() {
            console.log('scroll on');
            var sT = $(this).scrollTop();
            if (sT > 1) {
                $('.logo-block').addClass('affix');
            } else {
                $('.logo-block').removeClass('affix');
            }

            var sT2 = $(this).scrollTop();
            if (sT2 > 1) {
                $('.logo').addClass('affix');
            } else {
                $('.logo').removeClass('affix');
            }
        });
    }


    

    //menu height set to window.

    $(window).resize(function() {
        var siteMenu = $(window).height() - $('header').height();
        $("#site-menu").css("height", siteMenu);
    });

  
});

    
